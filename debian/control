Source: qpid-proton
Section: libs
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 cmake,
 debhelper-compat (= 10),
 dh-python,
 doxygen,
 libssl-dev,
 openstack-pkg-tools,
 pkgconf,
 python3-all,
 python3-all-dev,
 python3-sphinx,
 swig,
 uuid-dev,
Standards-Version: 3.9.8
Homepage: https://qpid.apache.org/proton/
Vcs-Browser: https://salsa.debian.org/openstack-team/third-party/qpid-proton
Vcs-Git: https://salsa.debian.org/openstack-team/third-party/qpid-proton.git

Package: libqpid-proton-cpp12
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Pre-Depends:
 ${misc:Pre-Depends},
Description: C++ libraries for Qpid Proton
 Qpid Proton is a high-performance, lightweight messaging library. It can be
 used in the widest range of messaging applications, including brokers, client
 libraries, routers, bridges, proxies, and more. Proton makes it trivial to
 integrate with the AMQP 1.0 ecosystem from any platform, environment, or
 language.
 .
 This package provides the C++ shared libraries for Qpid Proton.

Package: libqpid-proton-cpp12-dev
Provides:
 libqpid-proton-cpp-dev,
Architecture: any
Section: libdevel
Depends:
 libqpid-proton-cpp12 (= ${binary:Version}),
 libqpid-proton11-dev (= ${binary:Version}),
 ${misc:Depends},
Breaks:
 libqpid-proton-cpp8-dev,
Replaces:
 libqpid-proton-cpp8-dev,
Description: C++ Development libraries for writing messaging apps with Qpid Proton
 Qpid Proton is a high-performance, lightweight messaging library. It can be
 used in the widest range of messaging applications, including brokers, client
 libraries, routers, bridges, proxies, and more. Proton makes it trivial to
 integrate with the AMQP 1.0 ecosystem from any platform, environment, or
 language.
 .
 This package provides the C++ development libraries for Qpid Proton.

Package: libqpid-proton-cpp12-dev-doc
Architecture: all
Section: doc
Depends:
 libjs-jquery,
 ${misc:Depends},
Description: C++ developer documentation for Qpid Proton
 Qpid Proton is a high-performance, lightweight messaging library. It can be
 used in the widest range of messaging applications, including brokers, client
 libraries, routers, bridges, proxies, and more. Proton makes it trivial to
 integrate with the AMQP 1.0 ecosystem from any platform, environment, or
 language.
 .
 This package provides C++ developer documentation for Qpid Proton.

Package: libqpid-proton11
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Pre-Depends:
 ${misc:Pre-Depends},
Description: C libraries for Qpid Proton
 Qpid Proton is a high-performance, lightweight messaging library. It can be
 used in the widest range of messaging applications, including brokers, client
 libraries, routers, bridges, proxies, and more. Proton makes it trivial to
 integrate with the AMQP 1.0 ecosystem from any platform, environment, or
 language.
 .
 This package provides the shared libraries for Qpid Proton.

Package: libqpid-proton11-dev
Breaks:
 libqpid-proton2-dev,
 libqpid-proton8-dev,
Replaces:
 libqpid-proton2-dev,
 libqpid-proton8-dev,
Provides:
 libqpid-proton-dev,
Architecture: any
Section: libdevel
Depends:
 libqpid-proton11 (= ${binary:Version}),
 ${misc:Depends},
Description: C Development libraries for writing messaging apps with Qpid Proton
 Qpid Proton is a high-performance, lightweight messaging library. It can be
 used in the widest range of messaging applications, including brokers, client
 libraries, routers, bridges, proxies, and more. Proton makes it trivial to
 integrate with the AMQP 1.0 ecosystem from any platform, environment, or
 language.
 .
 This package provides the C development libraries for Qpid Proton.

Package: libqpid-proton11-dev-doc
Breaks:
 libqpid-proton2-dev-doc,
Replaces:
 libqpid-proton2-dev-doc,
Provides:
 libqpid-proton-dev-doc,
Architecture: all
Section: doc
Depends:
 libjs-jquery,
 ${misc:Depends},
Description: Developer documentation for Qpid Proton
 Qpid Proton is a high-performance, lightweight messaging library. It can be
 used in the widest range of messaging applications, including brokers, client
 libraries, routers, bridges, proxies, and more. Proton makes it trivial to
 integrate with the AMQP 1.0 ecosystem from any platform, environment, or
 language.
 .
 This package provides C developer documentation for Qpid Proton.

Package: libqpid-proton11-dev-examples
Architecture: all
Section: libdevel
Depends:
 libqpid-proton11-dev (>= ${binary:Version}),
 ${misc:Depends},
Breaks:
 libqpid-proton2-dev-examples,
Replaces:
 libqpid-proton2-dev-examples,
Provides:
 libqpid-proton-dev-examples,
Description: Example applications for writign messaging apps with Qpid Proton
 Qpid Proton is a high-performance, lightweight messaging library. It can be
 used in the widest range of messaging applications, including brokers, client
 libraries, routers, bridges, proxies, and more. Proton makes it trivial to
 integrate with the AMQP 1.0 ecosystem from any platform, environment, or
 language.
 .
 This package provides C and C++ language examples for Qpid Proton.

Package: python-qpid-proton-doc
Architecture: all
Section: doc
Depends:
 ${misc:Depends},
Description: Documentation for the Python language bindings for Qpid Proton
 Qpid Proton is a high-performance, lightweight messaging library. It can be
 used in the widest range of messaging applications, including brokers, client
 libraries, routers, bridges, proxies, and more. Proton makes it trivial to
 integrate with the AMQP 1.0 ecosystem from any platform, environment, or
 language.
 .
 This package provides documentation for the Python language bindings for
 Qpid Proton.

Package: python3-qpid-proton
Architecture: any
Section: python
Depends:
 libqpid-proton11,
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Provides:
 ${python3:Provides},
Description: language bindings for Qpid Proton messaging framework - Python 3.x
 Qpid Proton is a high-performance, lightweight messaging library. It can be
 used in the widest range of messaging applications, including brokers, client
 libraries, routers, bridges, proxies, and more. Proton makes it trivial to
 integrate with the AMQP 1.0 ecosystem from any platform, environment, or
 language.
 .
 This package provides Python 3.x language bindings for Qpid Proton.
